package encryption.nodes;

import encryption.actions.*;

public class NodeInitializer {
    public static Node initNodes() {
        Node start = new Node("Общий выбор");
        Node generalEncript = new Node("Зашифровка");
        Node decryption = new Node("Дешифровка");
        start.addChildren(generalEncript);
        start.addChildren(decryption);
        generalEncript.addAction(new XorEncryptAction("Зашифровка XOR методом"));
        generalEncript.addAction(new SimpleEncryptionAction("Простое Шифрование"));
        generalEncript.addAction(new CaesarEncryptionAction("Шифрование Цезаря"));
        decryption.addAction(new SimpleDecryptionAction("Дешифровка простого шифра"));
        return start;
    }
}
