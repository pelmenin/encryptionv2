package encryption.nodes;

import encryption.actions.Action;

import java.util.ArrayList;

public class Node {
    public ArrayList<Action> actions = new ArrayList<>();
    private Node parent;
    public ArrayList<Node> children = new ArrayList<>();
    private String title;

    public Node(String title) {
        this.title = title;
    }

    public Node(String title, Action action) {
        this.title = title;
        actions.add(action);
    }
    public String getTitle(){
        return title;
    }

    public ArrayList<Action> getAction() {
        return actions;
    }

    public void addAction(Action actions) {
        this.actions.add(actions);
    }

    public Node getParent(){
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public ArrayList<Node> getChildren() {
        return children;
    }

    public void addChildren(Node children) {
        this.children.add(children);
        children.parent = this;
    }

    public boolean hasParent() {
        return parent == null;
    }

    public int sizeChildren() {
        return children.size();
    }

    public int sizeAction() {
        return actions.size();
    }

    public void print() {
        if (hasParent()) {
            System.out.println("0: Выйти");
        }
        else {
            System.out.println("0: Вернуться назад ");
        }
        int i = 1;
        for (Node child : children) {
            System.out.format("%s: %s\n", i, child.getTitle());
            i++;
        }

        for (Action action : actions) {
            System.out.format("%s: %s\n", i, action.getTitle());
            i++;
        }
    }
}
