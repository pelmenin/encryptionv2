package encryption;

import java.util.Scanner;
import encryption.nodes.Node;

import static encryption.nodes.NodeInitializer.initNodes;

public class App {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int userInput = 0;
        int sizeChildren;
        Node currentNode = initNodes();

        while(currentNode != null) {
            sizeChildren = currentNode.sizeChildren();
            currentNode.print();
            userInput = sc.nextInt();
            System.out.println();
            if (userInput == 0) {
                currentNode = currentNode.getParent();
            } else if (userInput > 0 && userInput <= sizeChildren) {
                currentNode = currentNode.children.get(userInput - 1);
            } else if (userInput > sizeChildren && userInput <= sizeChildren + currentNode.sizeAction()) {
                currentNode.actions.get(userInput - sizeChildren - 1).doSmth();
            }
        }
    }
}
