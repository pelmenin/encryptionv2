package encryption.actions;

import java.util.Scanner;

public class SimpleEncryptionAction extends Action{

    int x = 0;

    public SimpleEncryptionAction(String title) {
        super(title);
    }

    @Override
    public void doSmth() {
        Scanner sc = new Scanner(System.in);
        String text = null;
        int key = 0;
        System.out.println("Введите текст");
        if (sc.hasNextLine()) {
            text = sc.nextLine();
        }
        System.out.println("Введите ключ");
        if (sc.hasNextLine()) {
            try {
                key = sc.nextInt();
            }
            catch (java.util.InputMismatchException e){
                System.out.println("Попробуйте еще раз");
            }
        }
        this.x = key;
        StringBuilder encrypted = new StringBuilder("");
        for (int i = 0; i < text.length(); i++) {
            encrypted.append((char) ((int) text.charAt(i) + distance()));
        }
        System.out.println("Зашифрованное сообщение: " + encrypted);
    }
    int distance() {
      return (this.x - 1) % Character.MAX_CODE_POINT;
    }
}
