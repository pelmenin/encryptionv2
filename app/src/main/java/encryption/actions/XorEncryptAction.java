package encryption.actions;

import java.util.Scanner;

public class XorEncryptAction extends Action{

    public XorEncryptAction(String title) {
        super(title);
    }

    @Override
    public void doSmth() {
        Scanner sc = new Scanner(System.in);
        String text = null;
        String key = null;
        System.out.println("Введите текст");
        if (sc.hasNextLine()) {
            text = sc.nextLine();
        }
        System.out.println("Введите ключ");
        if (sc.hasNextLine()) {
            key = sc.nextLine();
        }
        int[] output = new int[text.length()];
        for(int i = 0; i < text.length(); i++) {
            int o = (Integer.valueOf(text.charAt(i)) ^ Integer.valueOf(key.charAt(i % (key.length() - 1)))) + '0';
            output[i] = o;
        }
        System.out.print("Зашифрованное сообщение: ");
        for (int i = 0; i < output.length; i++) {
            System.out.print(output[i] + " ");
        }
        System.out.println();
    }
}