package encryption.actions;

import java.util.Scanner;

public class CaesarEncryptionAction extends Action{

  protected int rot;
  protected String ruAlpha= "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
  protected String enAlpha = "abcdefghijklmnopqrstuvwxyz";
    int x = 0;
    public CaesarEncryptionAction(String title) {
        super(title);
    }

    @Override
    public void doSmth() {
        Scanner sc = new Scanner(System.in);
        String text = null;
        int key = 0;
        System.out.println("Введите текст");
        if (sc.hasNextLine()) {
            text = sc.nextLine();
        }
        System.out.println("Введите ключ");
        if (sc.hasNextLine()) {
            try {
                key = sc.nextInt();
            }
            catch (java.util.InputMismatchException e){
                System.out.println("Попробуйте еще раз");
            }
        }
//        StringBuilder encrypted = new StringBuilder("");
        StringBuffer result= new StringBuffer();

        for (int i=0; i<text.length(); i++)
        {
            if (Character.isUpperCase(text.charAt(i)))
            {
                char ch = (char)(((int)text.charAt(i) +
                        x - 65) % 26 + 65);
                result.append(ch);
            }
            else
            {
                char ch = (char)(((int)text.charAt(i) +
                        x - 97) % 26 + 97);
                result.append(ch);
            }
        }
        System.out.println("Зашифрованное сообщение: " + result);
    }
}
